function economicalBowlers(matchesData, deliveriesData, year){
    let bowlers = {}
    for (let match of matchesData){
        if (year == match.season){
            let id = match.id
            for (let delivery of deliveriesData){
                if (delivery.match_id == id ){
                    if (delivery.wide_runs == 0 && delivery.noball_runs == 0){

                        if (bowlers[delivery.bowler]){
                            bowlers[delivery.bowler].totalBalls += 1
                            bowlers[delivery.bowler].totalRuns += parseFloat(delivery.total_runs)
                            //let noOfRun = bowlers[delivery.bowler].totalRuns;
                            //let noOfBall = bowlers[delivery.bowler].totalBalls;
    
                        }
                        else {

                            bowlers[delivery.bowler] = {"totalRuns": parseFloat(delivery.total_runs), "totalBalls": 1, "economy": 0}
                        }
                    }
                }
            }
        }  
    }

let topten = []
for (let eachBowler in bowlers){
     topten.push(bowlers[eachBowler].economy)  
}
topten.sort(function(a,b){
    return a-b;
})
let myobect = {}
for (let j=0; j < 10; j++){
    for(let bowler in bowlers){
        if (bowlers[bowler].economy == topten[j]){
            myobect[bowler] = topten[j]
        }
    }
}
return myobect
}
module.exports = economicalBowlers;

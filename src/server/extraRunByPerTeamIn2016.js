function extraRunByPerTeam(matchesData,deliveriesData,year) {
    let ExtraRunByTeams = {}
    for(let match of matchesData){
        if(year === match.season){
            let id = match.id
            for(let delivery of deliveriesData){
                if(delivery.match_id === id){
                    if(ExtraRunByTeams[delivery.bowling_team]){
                        ExtraRunByTeams[delivery.bowling_team] += parseInt(delivery.extra_runs)
                    }
                    else{
                        ExtraRunByTeams[delivery.bowling_team] = parseInt(delivery.extra_runs)
                    }
                }
            }
        }
    }
    console.log(ExtraRunByTeams)
    return ExtraRunByTeams;

}
module.exports = extraRunByPerTeam;

function matchesWonPerYear(matches){
    let matchesPerYear = {}
    for (let index = 0; index < matches.length; index++){
        let year = matches[index].season
        let winningTeam = matches[index].winner
        if (matchesPerYear[winningTeam]) {
            if (matchesPerYear[winningTeam][year]){
                matchesPerYear[winningTeam][year] += 1 
            }
            else {
                
                    matchesPerYear[winningTeam][year] = 1 
            }
            
        }
        else {
            let initialValue = [[year, 1]]
            matchesPerYear[winningTeam] = Object.fromEntries(initialValue)
        }
    }
    return matchesPerYear;
}
module.exports = matchesWonPerYear;

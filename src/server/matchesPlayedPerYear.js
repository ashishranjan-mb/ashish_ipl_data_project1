function matchesPlayedPerYear(matches) {
    let matchPerYear = {}
    for(let index = 0; index < matches.length; index++){
        let year = (matches[index].season)
        if(matchPerYear[year]) {
            matchPerYear[year] += 1;
        }
        else
        {
            matchPerYear[year] = 1;
        }
    }
    return matchPerYear;
}
module.exports = matchesPlayedPerYear;


